<?php

namespace App\Models;

use \PDO;
use stdClass;

class EvenementModel extends SqlConnect {
    public function add(array $data) {
      $query = "
      INSERT INTO `EVENEMENTS` (`nom`, `description`, `date_debut`, `date_fin`, `lieu`, `photo_couverture`, `confidentialite`) 
      VALUES (:nom, :description, :date_debut, :date_fin, :lieu, :photo_couverture, :confidentialite)
      ";

      $req = $this->db->prepare($query);
      $req->execute($data);
    }

    public function delete(int $id) {
      $req = $this->db->prepare("DELETE FROM EVENEMENTS WHERE id_evenement = :id");
      $req->execute(["id" => $id]);
    }

    public function get(int $id) {
      $req = $this->db->prepare("SELECT * FROM EVENEMENTS WHERE id_evenement = :id");
      $req->execute(["id" => $id]);

      return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
    }

    public function getLast() {
      $req = $this->db->prepare("SELECT * FROM EVENEMENTS ORDER BY id_evenement DESC LIMIT 1");
      $req->execute();

      return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
    }

    public function getAllEvenementsFromParis() {
      $req = $this->db->prepare("SELECT * FROM EVENEMENTS WHERE lieu = 'Paris'");
      
      $req->execute();
      return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : [];
    }

    public function getAllEvenementWithPhotos() {
      $req = $this->db->prepare("SELECT e.* 
      FROM EVENEMENTS e
      INNER JOIN ALBUM_PHOTOS ap ON e.id_evenement = ap.id_evenement
      INNER JOIN PHOTOS p ON ap.id_album_photo = p.id_album_photo
      GROUP BY e.id_evenement
      HAVING COUNT(p.id_photo) >= 3");
      
      $req->execute();
      return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : [];
    }

    public function getAllEvenementsWith10Participations() {
      $req = $this->db->prepare("SELECT e.*
      FROM EVENEMENTS e 
      INNER JOIN participer_evenements pe ON e.id_evenement = pe.id_evenement
      GROUP BY e.id_evenement
      HAVING COUNT(DISTINCT pe.id_utilisateur) > 10");
      
      $req->execute();
      return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : [];
    }

    public function getNbBilletsVendusForPlus1000Euros() {
      $req = $this->db->prepare("SELECT e.nom, COUNT(ab.id_billet) AS nb_billets_vendus, AVG(tb.montant) AS prix_moyen
      FROM EVENEMENTS e
      LEFT JOIN BILLETERIES b ON e.id_evenement = b.id_evenement  
      LEFT JOIN TYPES_BILLETS tb ON b.id_billetterie = tb.id_billetterie
      LEFT JOIN BILLETS bi ON tb.id_type_billet = bi.id_type_billet
      LEFT JOIN acheter_billets ab ON bi.id_billet = ab.id_billet
      GROUP BY e.id_evenement");
      
      $req->execute();
      return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : [];
    }

    public function getAllEvenementsAndNbBilletsVendus() {
      $req = $this->db->prepare("SELECT e.nom AS titre_evenement, 
        SUM(tb.quantite) AS nombre_billets_vendus, 
        SUM(tb.montant * tb.quantite) AS chiffre_affaires
      FROM EVENEMENTS e
      JOIN BILLETERIES b ON e.id_evenement = b.id_evenement
      JOIN TYPES_BILLETS tb ON b.id_billetterie = tb.id_billetterie
      GROUP BY e.id_evenement
      HAVING SUM(tb.montant * tb.quantite) > 1000");
      
      $req->execute();
      return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : [];
    }

    public function getAllEvenementWhereLieuIsNotNull() {
      $req = $this->db->prepare("SELECT DISTINCT lieu
      FROM EVENEMENTS
      WHERE lieu IS NOT NULL");
      
      $req->execute();
      return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : [];
    }

    public function getAllEvenementIn2021() {
      $req = $this->db->prepare("SELECT *
      FROM EVENEMENTS
      WHERE YEAR(date_debut) = 2021");
      
      $req->execute();
      return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : [];
    }

    public function updateEvenementFeteDeLaMusique() {
      $req = $this->db->prepare("UPDATE EVENEMENTS
      SET date_debut = DATE_ADD(date_debut, INTERVAL 7 DAY),
          date_fin = DATE_ADD(date_fin, INTERVAL 7 DAY)
      WHERE nom = 'Fête de la musique'");
      
      $req->execute();
    }
}