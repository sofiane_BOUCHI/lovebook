<?php

namespace App\Models;

use \PDO;
use stdClass;

class UserModel extends SqlConnect {
    public function add(array $data) {
      $query = "
        INSERT INTO UTILISATEURS (email, nom, prenom, date_naissance, genre, photo_profil, role)
        VALUES (:email, :nom, :prenom, :date_naissance, :genre, :photo_profil, :role)
      ";

      $req = $this->db->prepare($query);
      $req->execute($data);
    }

    public function delete(int $id) {
      $req = $this->db->prepare("DELETE FROM UTILISATEURS WHERE id_utilisateur = :id");
      $req->execute(["id" => $id]);
    }

    public function get(int $id) {
      $req = $this->db->prepare("SELECT * FROM UTILISATEURS WHERE id_utilisateur = :id");
      $req->execute(["id" => $id]);

      return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
    }

    public function getLast() {
      $req = $this->db->prepare("SELECT * FROM UTILISATEURS ORDER BY id_utilisateur DESC LIMIT 1");
      $req->execute();

      return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
    }

    public function getAllUsersWithNoParticipations() {
      $req = $this->db->prepare("SELECT u.*
      FROM UTILISATEURS u
      LEFT JOIN participer_evenements pe ON u.id_utilisateur = pe.id_utilisateur
      GROUP BY u.id_utilisateur
      HAVING COUNT(pe.id_evenement) = 0");
      
      $req->execute();
      return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : [];
    }

    public function updateUtilisateursVille() {
      $req = $this->db->prepare("UPDATE UTILISATEURS
      SET ville = 'Marseille'
      WHERE nom = 'Dupont' AND prenom = 'Jean'");
      
      $req->execute();
    }
}