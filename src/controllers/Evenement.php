<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\Evenementmodel;

class Evenement extends Controller {
  protected object $evenement;

  public function __construct($param) {
    $this->evenement = new Evenementmodel();

    parent::__construct($param);
  }

  public function postEvenement() {
    if (($this->body) == null) {
      // Requete 9
      $this->evenement->updateEvenementFeteDeLaMusique();
    } else{
      $this->evenement->add($this->body);
    }
    return $this->evenement->getLast();
  }

  public function deleteEvenement() {
    return $this->evenement->delete(intval($this->params['id']));
  }

  
  public function getEvenement() {
    // echo ($this->params['id']) ;
    if (($this->params['id']) == 'evenementsParis') {
      // Requete 1
      return $this->evenement->getAllEvenementsFromParis();

    } elseif(($this->params['id']) == 'evenementsWithPhotos') {
      // Requete 2
      return $this->evenement->getAllEvenementWithPhotos();

    } elseif(($this->params['id']) == 'evenementsWith10Participations') {
      // Requete 3
      return $this->evenement->getAllEvenementsWith10Participations();

    } elseif(($this->params['id']) == 'nbBilletsVendusForPlus1000Euros') {
      // Requete 4
      return $this->evenement->getNbBilletsVendusForPlus1000Euros();

    } elseif(($this->params['id']) == 'evenementsAndNbBilletsVendus') {
      // Requete 5
      return $this->evenement->getAllEvenementsAndNbBilletsVendus();

    } elseif(($this->params['id']) == 'lieuNonNull') {
      // Requete 6
      return $this->evenement->getAllEvenementWhereLieuIsNotNull();

    } elseif(($this->params['id']) == 'evenementsIn2021') {
      // Requete 7
      return $this->evenement->getAllEvenementIn2021();
      
    } else{
      return $this->evenement->get(intval($this->params['id']));
    }
    
  }
  

}
