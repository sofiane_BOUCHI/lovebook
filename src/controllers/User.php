<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\Usermodel;

class User extends Controller {
  protected object $user;

  public function __construct($param) {
    $this->user = new Usermodel();

    parent::__construct($param);
  }

  public function postUser() {
    if (($this->body) == null) {
      // Requete 10
      $this->user->updateUtilisateursVille();
    } else{
      $this->user->add($this->body);
    }
    return $this->user->getLast();
  }

  public function deleteUser() {
    return $this->user->delete(intval($this->params['id']));
  }

  public function getUser() {
    // echo ($this->params['id']) ;
    if (($this->params['id']) == 'noParticipations') {
      // Requete 8
      return $this->user->getAllUsersWithNoParticipations();
    } else{
      return $this->user->get(intval($this->params['id']));
    }
    
  }
}
