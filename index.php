<?php

require 'vendor/autoload.php';

use App\Router;
use App\Controllers\User;
use App\Controllers\Evenement;

new Router([
  'user/:id' => User::class,
  'user/add' => User::class,
  
  'evenement/:id' => Evenement::class,
  'evenement/add' => Evenement::class,
]);